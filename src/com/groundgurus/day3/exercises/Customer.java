package com.groundgurus.day3.exercises;

public class Customer {
	private String name;
	private double cash;
	private Cart cart;
	
	public Customer(String name, double cash) {
		this.name = name;
		this.cash = cash;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getCash() {
		return cash;
	}

	public void setCash(double cash) {
		this.cash = cash;
	}

	public Cart getCart() {
		return cart;
	}

	public void setCart(Cart cart) {
		this.cart = cart;
	}
}
