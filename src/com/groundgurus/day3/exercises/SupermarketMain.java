package com.groundgurus.day3.exercises;

public class SupermarketMain {
	public static void main(String[] args) {
		Supermarket supermarket = new Supermarket();
		// load the items in the supermarket
		supermarket.loadItems();
		
		Customer john = new Customer("John Doe", 500.00);
		Cart smallCart = new Cart();
		john.setCart(smallCart);
		
		smallCart.addToCart(supermarket, "Eggs");
		smallCart.addToCart(supermarket, "Butter (small)");
		smallCart.addToCart(supermarket, "Pack of Bread");
		
		supermarket.checkout(john);
	}
}
