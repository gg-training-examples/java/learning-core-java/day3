package com.groundgurus.day3.exercises;

public class Supermarket {
	Item[] items = new Item[1_000];
	
	public void loadItems() {
		items[0] = new Item("Eggs", 90.00, 12);
		items[1] = new Item("Butter (small)", 20.00, 1);
		items[2] = new Item("Pack of Bread", 40.00, 1);
	}
	
	// search the item by name
	public Item getItem(String itemName) {
		for (int i = 0; i < items.length; i++) {
			// get the item
			Item item = items[i];
			
			// if the item is found return it and remove it
			// on the supermarket
			if (item != null && item.name.equals(itemName)) {
				items[i] = null;
				return item;
			}
		}
		
		return null;
	}
	
	public void checkout(Customer customer) {
		double change = 0.0;
		double totalPrice = 0.0;
		
		System.out.println("Customer: " + customer.getName());
		System.out.println("Cash: Php " + customer.getCash());
		
		Item[] itemsBought = customer.getCart().getItems();
		
		// print the items bought
		System.out.println("Items Bought:");
		for (Item item : itemsBought) {
			if (item != null) {
				System.out.println("- " + item.name + ", Php " + item.price);
			}
		}
		
		// compute the total price
		for (Item item : itemsBought) {
			if (item != null) {
				totalPrice += item.price;
			}
		}
		
		change = customer.getCash() - totalPrice;
		//customer.cash = customer.cash - totalPrice;
		customer.setCash(customer.getCash() - totalPrice);
		
		System.out.println("Change: " + change);
	}
}
