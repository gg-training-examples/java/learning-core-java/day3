package com.groundgurus.day3.exercises;

public class Cart {
	private Item[] items = new Item[100];
	private int count = 0;
	
	public void addToCart(Supermarket supermarket,
			String itemName) {
		items[count++] = supermarket.getItem(itemName);
	}
	
	public Item[] getItems() {
		return items;
	}
}
