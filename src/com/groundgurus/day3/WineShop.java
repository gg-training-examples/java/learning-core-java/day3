package com.groundgurus.day3;

/*
 * WineShop - child, subclass
 * Shop     - parent, superclass
 */
public class WineShop extends Shop {
	boolean areMinorsAllowed;
	
	public WineShop(String name, String[] items, boolean areMinorsAllowed) {
		super(name, items);
		this.areMinorsAllowed = areMinorsAllowed;
	}
	
	// method overriding
	@Override
	public void printDetails() {
		super.printDetails();
		System.out.println("Are Minors Allowed? " 
				+ areMinorsAllowed);
	}
	
}
