package com.groundgurus.day3;

import java.util.Arrays;

public class ShopMain {
	public static void main(String[] args) {
		// one way to initialize an object with arguments
		String cakeShopName = "Red Ribbon";
		String[] cakeShopItems = new String[] { 
				"Triple Chocolate", "Black Forest" 
		};
		Shop cakeShop = new Shop(cakeShopName, cakeShopItems);
		
		// another way to initialize an object with arguments
		// Arrays.asList() returns an ArrayList (day 3)
		// .toArray(new String[0]) converts ArrayList to
		// an array of String
		Shop flowerShop = new Shop("Daisy's Flowery",
				Arrays.asList("Red Flower", "Blue Flower")
				.toArray(new String[0]));
		
		cakeShop.printDetails();
		
		System.out.println();
		
		flowerShop.printDetails();
		
		System.out.println();
		
		WineShop wineShop = new WineShop("Joe's Winery", 
				new String[] {"Wine 1"}, false);
		wineShop.printDetails();
	}
}
